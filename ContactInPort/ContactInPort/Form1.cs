﻿using ContactInPort.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContactInPort
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var html = this.txtHtml.Text;
                //var abc = new PersonHelper().GetRawDataPerson(html);
                //Model.Person p1 = new Model.Person("name1", "Com1", "email1", "web1", "tell1");
                //Model.Person p2 = new Model.Person("name1", "Com1", "email1", "web1", "tell1");
                //Model.Person p3 = new Model.Person("name1", "Com1", "email1", "web1", "tell1");
                //Model.Person p4 = new Model.Person("name1", "Com1", "email1", "web1", "tell1");
                //List<Model.Person> people = new List<Model.Person>();
                //people.Add(p1);
                //people.Add(p2);
                //people.Add(p3);
                //people.Add(p4);
                //new ExcelHelper().Write(people);
                //return;
                new PersonHelper().GetPerson(html);
                this.txtCount.Text = Session.MySesssion.ses_peo.Count().ToString();
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                List<Model.Person> people = Session.MySesssion.ses_peo;
                new ExcelHelper().Write(people);
                MessageBox.Show("Viết excel thành công");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Xóa dữ liệu nhé", "Are you sure?", MessageBoxButtons.YesNoCancel); //Gets users input by showing the message box

            if (result == DialogResult.Yes) //Creates the yes function
            {
                Session.MySesssion.ses_peo = new List<Model.Person>(); ; //Exits off the application
                MessageBox.Show("Đã xóa dữ liệu");
                this.txtCount.Text = Session.MySesssion.ses_peo.Count().ToString();
            }

            else if (result == DialogResult.No)
            {
                //Does nothing
            }
            
           
        }
    }
}
