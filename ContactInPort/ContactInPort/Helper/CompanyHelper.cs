﻿using ContactInPort.Model;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactInPort.Helper
{
    class CompanyHelper
    {
        
        public Model.Company GetCompanyFromRawData(String html)
        {
            List<HtmlNode> people = new List<HtmlNode>();
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            //var htmlweb = MyHtmlWeb.Init();
            //var abc = htmlweb.LoadHtml("abc.html");
            var def = doc.DocumentNode.SelectNodes("//div[@id='contactperson']/div[@class='col-md-12']/div[@class='contactperson_row']/div[@class='contactperson_info row col-xs-12 col-sm-9']");

            Company company = convertDTOtoCompany(this.GetCompanyDTO(html));
            company.name = this.getName(html);
            return company;

        }
        public CompanyDTO GetCompanyDTO(String html)
        {
            CompanyDTO companyDTO = new CompanyDTO();
            List<Item> items = new List<Item>();
            var listLabel = this.getListProfileLable(html);
            var listValue = this.getListProfileValue(html);
            for(int i = 0; i < listLabel.Count; i++)
            {
                Item item = new Item();
                item.key = listLabel[i];
                item.value = listValue[i];
                items.Add(item);
            }
            companyDTO.items = items;
            return companyDTO;
        }
        public Company convertDTOtoCompany(CompanyDTO dto)
        {
            Company company = new Company();
            for(int i = 0; i < dto.items.Count; i ++)
            {
                if(dto.items[i].key.Contains("Email"))
                {
                    company.mail = dto.items[i].value;
                }
                if(dto.items[i].key.Contains("Website"))
                {
                    company.website = dto.items[i].value;
                }    
                if(dto.items[i].key.Contains("Phone") || dto.items[i].key.Contains("Tell"))
                {
                    company.tell = dto.items[i].value;
                }    else if(dto.items[i].key.Contains("Mobile Phone")|| dto.items[i].key.Contains("Line"))
                {
                    company.tell = dto.items[i].value;
                }
            };
            return company;
        }
        public String getName(String html)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            return doc.DocumentNode.SelectNodes("//div[@class='company']").First().InnerText.Trim();

        }
        public List<String> getListProfileLable(String html)
        {
            List<String> ListProfileLable = new List<string>();
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            var profile_row = doc.DocumentNode.SelectNodes("//div[@class='profile_row']/div[@class='col-sm-3 col-xs-12 profile_label']");
            foreach(var row in profile_row)
            {
                ListProfileLable.Add(row.InnerText);
            }
            return ListProfileLable;
        }
        public List<String> getListProfileValue(String html)
        {
            List<String> ListProfileLable = new List<string>();
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            var profile_row = doc.DocumentNode.SelectNodes("//div[@class='profile_row']/div[@class='col-sm-9 col-xs-12 profile_val']");
            foreach (var row in profile_row)
            {
                ListProfileLable.Add(row.InnerText.Trim());
            }
            return ListProfileLable;
        }
    }
}
