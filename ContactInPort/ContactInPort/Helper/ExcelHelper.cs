﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
namespace ContactInPort.Helper
{
    class ExcelHelper
    {
        public void Write(List<Model.Person> people)
        {
            Excel.Application excelApp = new Excel.Application();
            if (excelApp != null)
            {
                Excel.Workbook excelWorkbook = excelApp.Workbooks.Add();
                Excel.Worksheet excelWorksheet = (Excel.Worksheet)excelWorkbook.Sheets.Add();
                people.ForEach(p =>
                {
                    excelWorksheet.Cells[people.IndexOf(p)+1, 2] = p.Name;
                    excelWorksheet.Cells[people.IndexOf(p) + 1, 3] = p.CompanyName;
                    excelWorksheet.Cells[people.IndexOf(p) + 1, 4] = p.Email;
                    excelWorksheet.Cells[people.IndexOf(p) + 1, 5] = p.website;
                    excelWorksheet.Cells[people.IndexOf(p) + 1, 6] = "\u0022" + p.tell+ "\u0022";
                });

                String path = ConfigurationSettings.AppSettings["excelpath"];
                excelApp.ActiveWorkbook.SaveAs(path+@"\Contact.xls", Excel.XlFileFormat.xlWorkbookNormal);

                excelWorkbook.Close();
                excelApp.Quit();

                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelWorksheet);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelWorkbook);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelApp);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}
