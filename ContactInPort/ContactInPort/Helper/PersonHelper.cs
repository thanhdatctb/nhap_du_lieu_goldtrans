﻿using ContactInPort.Model;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContactInPort.Helper
{
    class PersonHelper
    {
        public List<Person> GetPerson(String html)
        {
            List<Person> people = new List<Person>();
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            //var htmlweb = MyHtmlWeb.Init();
            //var abc = htmlweb.LoadHtml("abc.html");
            var def = doc.DocumentNode.SelectNodes("//div[@id='contactperson']/div[@class='col-md-12']/div[@class='contactperson_row']/div[@class='contactperson_info row col-xs-12 col-sm-9']");
            Company company = new CompanyHelper().GetCompanyFromRawData(html);
            List<String> listLabel = new List<string>();
            List<String> listValue = new List<string>();
            foreach (var i in def)
            {
                listLabel = this.getListLabel(i);
                listValue = this.getListValue(i);
                
            }
            var dto = this.GetPersonDTO(listLabel, listValue);
            var dtos = this.SplitDTO(dto);
            
            foreach(var d in dtos)
            {
                Person person = this.GetPersonFromDto(d);
                person.CompanyName = company.name;
                person.website = company.website;
                if(person.tell == null)
                {
                    person.tell = company.tell;
                }   
                if(person.Email == null)
                {
                    person.Email = company.mail;
                }    
                people.Add(person);
                Session.MySesssion.ses_peo.Add(person);

            }    
            //foreach(var i in def)
            //{
            //    var person = new Model.Person();
            //    getPersonFormRawData(i);
                
               
            //    //MessageBox.Show(i.InnerText.ToString());
            //    people.Add(i);
            //}
            
            return people;
            
        }
        public List<PersonDTO> SplitDTO(PersonDTO oldDTO)
        {
            List<PersonDTO> NewDtos = new List<PersonDTO>();
            int first = 0;
            for(int i = 0; i < oldDTO.items.Count; i++)
            {
                if((oldDTO.items[i].key.Contains("name") ||
                    oldDTO.items[i].key.Contains("Name")|| i == oldDTO.items.Count-1)&&i!=0)
                {
                    PersonDTO dto = this.getListwithIndex(oldDTO, first, i - 1);
                    NewDtos.Add(dto);
                    first = i;
                }
            }    
            return NewDtos;
        }
        private PersonDTO getListwithIndex(PersonDTO people, int first, int last)
        {
            List<PersonDTO> list = new List<PersonDTO>();
            PersonDTO dto = new PersonDTO();
            List<Item> items = new List<Item>();
            for (int i = first; i<=last; i++)
            {
                
                items.Add(people.items[i]);
               
                
                
            }
            dto.items = items;
            return dto;
        }
        public PersonDTO GetPersonDTO(List<String> listLabel, List<String> listValue)
        {
            PersonDTO personDTO = new PersonDTO();
            List<Item> items = new List<Item>();
            for (int i = 0; i < listLabel.Count; i++)
            {
                Item item = new Item();
                item.key = listLabel[i];
                item.value = listValue[i];
                items.Add(item);
            }
            personDTO.items = items;
            return personDTO;
        }
        public Person GetPersonFromDto(PersonDTO dto)
        {
            Person person = new Person();
            for (int i = 0; i < dto.items.Count; i++)
            {
                if (dto.items[i].key.Contains("mail"))
                {
                    person.Email = dto.items[i].value;
                }
                
                if (dto.items[i].key.Contains("Company name"))
                {
                    person.CompanyName = dto.items[i].value;
                }else if (dto.items[i].key.Contains("name")|| dto.items[i].key.Contains("Name"))
                {
                    person.Name = dto.items[i].value;
                }
                if (dto.items[i].key.Contains("Website"))
                {
                    person.website = dto.items[i].value;
                }
                if (dto.items[i].key.Contains("Phone") || dto.items[i].key.Contains("Tell"))
                {
                    person.tell = dto.items[i].value;
                }
                else if (dto.items[i].key.Contains("Mobile Phone") || dto.items[i].key.Contains("Line"))
                {
                    person.tell = dto.items[i].value;
                }
            };
            return person;
        }
        public Person getPersonFormRawData(HtmlNode node)
        {
            Person person = new Person();

            //MessageBox.Show(node.InnerHtml.ToString());
            //return person;
            //var data = node.SelectNodes("//div[@class='contactperson_info row col-xs-12 col-sm-9']/div[@class='profile_row row']");
            //foreach (var d in data)
            //{
            //    MessageBox.Show(d.InnerHtml);
            //    var s = d.SelectNodes("//div[@class='col-sm-5 col-xs-12 profile_label']");
            //    List<string> listLable = new List<string>();
            //    List<string> listValue = new List<string>();
            //    foreach (var label in s)
            //    {
            //        listLable.Add(label.InnerText);

            //    }
            //    listLable.ForEach(m =>
            //    {
            //        MessageBox.Show(m);
            //    });
            //    MessageBox.Show("Hết 1 cái");


            //}


            return person;
        }
        private List<String> getListLabel(HtmlNode node)
        {
            List<string> listLable = new List<string>();
            List<string> listValue = new List<string>();
            //MessageBox.Show(node.InnerText);
            var data = node.SelectNodes("//div[@class='col-sm-5 col-xs-12 profile_label']");
            foreach (var d in data)
            {
                //MessageBox.Show(d.InnerHtml);
                
                 listLable.Add(d.InnerText);

            }
            return listLable;
        }
        private List<String> getListValue(HtmlNode node)
        {
            List<string> listLable = new List<string>();
            List<string> listValue = new List<string>();
            var data = node.SelectNodes("//div[@class='profile_row row']//div[@class='col-sm-7 col-xs-12 profile_val']");
            foreach (var d in data)
            {
                //MessageBox.Show(d.InnerHtml);
                
                
                listLable.Add(d.InnerText.Trim());
               

            }
           
            return listLable;
        }
    }
}
