﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactInPort.Model
{
    class Company
    {
        public String name { get; set; }
        public String mail { get; set; }
        public String website { get; set; }
        public String tell { get; set; }
    }
}
