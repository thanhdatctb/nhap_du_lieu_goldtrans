﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactInPort.Model
{
    class MyHtmlWeb
    {
        private static HtmlWeb htmlWeb = null;
        public static HtmlWeb Init()
        {
            if (htmlWeb == null)
            {
                htmlWeb = new HtmlWeb()
                {
                    AutoDetectEncoding = true,
                    OverrideEncoding = Encoding.UTF8  //Set UTF8 để hiển thị tiếng Việt
                };
            }
            return htmlWeb;
        }
    }
}
