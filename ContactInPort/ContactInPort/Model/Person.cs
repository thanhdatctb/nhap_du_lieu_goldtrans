﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactInPort.Model
{
    public class Person
    {
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string website { get; set; }
        public string tell { get; set; }

        public Person(string name, string companyName, string email, string website, string tell)
        {
            Name = name;
            CompanyName = companyName;
            Email = email;
            this.website = website;
            this.tell = tell;
        }
        public Person() { }
    }
}
